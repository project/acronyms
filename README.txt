Acronyms is a small module which handles Acronyms (also known as Abbreviations) on your site. The primary function is acronyms_parse_string($string) which takes and returns a string, properly formatted with the acronyms (or abbreviations) in it.

For example, say you have the following strings:

"The USA has 48 contiguous states." will output the same string, but save "USA" as a new acronym (if enabled in the acronyms->settings page)
"THE USA HAS 48 CONTIGUOUS STATES." will output "The USA has 48 Contiguous States." (if the acronym was added manually or automatically).
"the usa has 48 contiguous states." will output "The USA has 48 Contiguous States." (same reason as above.)

/******************** INSTALLATION ***********************/
1. Copy the "acronyms" folder into your appropriate modules folder (sites/all/modules, sites/default/modules, etc.)

2. Go to "Site Configuration >> Modules" and enable the Acronyms Module.

3. Configure the module settings on "Site Configuration >> Acronyms"

   * Enable Acronym Filtering - easily turn on/off the acronym filtering without having to enable/disable the module. This MUST BE CHECKED in order for the Acronyms module to filter/parse your strings.

   * Automatically Create Acronyms - will look for acronyms inside of your strings and automatically add them as appropriate. (NOTE: You can still edit/delete any erroneous ones on the main Acronyms Settings page).

   * Add New Acronym - Manually add a new acronym, which will be used in the filter/parsing process.
